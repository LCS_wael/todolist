<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">

        <title>TodoList</title>
        <link rel="stylesheet" href="/css/app.css">
    </head>
    <body>
        @include('inc.navbar')
        <div class="container">
            @include('inc.messages')
            @yield('content')
        </div>
        <footer id="footer" class="text-center fixed-bottom  bg-dark">

           Copyright &copy; 2020 TodoList

        </footer>

    </body>
</html>

